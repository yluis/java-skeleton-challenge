package com.qlouder;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;

import com.google.api.server.spi.config.Named;
import com.google.api.server.spi.config.Nullable;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.CollectionResponse;
import com.google.api.server.spi.response.NotFoundException;
import com.google.cloud.storage.*;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateException;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

import java.util.ArrayList;
import java.util.Collection;

import static java.nio.charset.StandardCharsets.UTF_8;


@Api(name = "api", version = "v1")
public class Endpoint {
    private Translate translate = TranslateOptions.getDefaultInstance().getService();
    public Storage storage = StorageOptions.getDefaultInstance().getService();
    public String bucketName = "qlouder-files-bucket";
    private String defaultOutputLanguage = "en";

    @ApiMethod(path = "files", httpMethod = "GET")
    public CollectionResponse<String> getFiles() {
        Collection<String> result = new ArrayList<>();

        Bucket bucket = storage.get(bucketName);
        for (Blob blob : bucket.list().iterateAll()) {
            result.add(blob.getName());
        }

        return CollectionResponse.<String>builder().setItems(result).build();
    }

    @ApiMethod(path = "files/{filename}", httpMethod = "GET")
    public FileTranslatorResponse getFile(@Named("filename") String filename) throws NotFoundException {
        FileTranslatorResponse response = new FileTranslatorResponse();
        BlobId blobId = BlobId.of(bucketName, filename);

        try {
            byte[] content = storage.readAllBytes(blobId);
            response.setResponse(new String(content, UTF_8));
        } catch (StorageException se) {
            throw new NotFoundException(String.format("file %s not found", filename));
        }
        return response;
    }

    @ApiMethod(path = "translate/{filename}", httpMethod = "POST")
    public FileTranslatorResponse translate(@Named("filename") String filename, @Named("language") @Nullable String language) throws NotFoundException, BadRequestException {

        FileTranslatorResponse response = new FileTranslatorResponse();
        BlobId blobId = BlobId.of(bucketName, filename);

        try {
            byte[] content = storage.readAllBytes(blobId);
            String contentString = new String(content, UTF_8);

            Translation translation = translate.translate(contentString,
                    TranslateOption.sourceLanguage("nl"),
                    TranslateOption.targetLanguage(language == null ? defaultOutputLanguage : language));

            String translatedFileName = String.format("%s-%s.txt", filename.split("\\.")[0], language == null ? defaultOutputLanguage : language);

            BlobId newBlobId = BlobId.of(bucketName, translatedFileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(newBlobId).setContentType("text/plain").build();
            storage.create(blobInfo, translation.getTranslatedText().getBytes(UTF_8));

            response.setResponse(String.format("created translation file: %s", translatedFileName));
        } catch (StorageException se) {
            throw new NotFoundException(String.format("file %s not found", filename));
        } catch (TranslateException te) {
            throw new BadRequestException(String.format("language %s not found", language));
        }
        return response;
    }
}
